import java.util.Arrays;

class Solution {
    public boolean check(int[] nums) {
        int n = nums.length;
        
        int[] sorted = Arrays.copyOf(nums, n);
        Arrays.sort(sorted);

        for (int i = 0; i < n; i++) {
            boolean isSorted = true;

            for (int j = 0; j < n; j++) {
                int current = nums[(i + j) % n];
                int expected = sorted[j];

                if (current != expected) {
                    isSorted = false;
                    break;
                }
            }

            if (isSorted) {
                return true;
            }
        }

        return false;
    }
}
